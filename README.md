# Contributing to AIcrowd Evaluation Metrics
🙌 We thank you for your interest in contributing to AIcrowd Evaluation Metrics! Before you start writing the code, let us go through this checklist. This checklist not only helps you to contribute quality code but also us to get your intention behind the contribution.
## Opening an issue
💭 Before requesting an additon to the project or an omission to the project, it is suggested to start a discussion in form of an [issue](https://gitlab.aicrowd.com/aicrowd/evaluation-metrics-book/-/issues). Herein, we shall discuss the design and implementation of the said change, and once it looks good to go, go ahead and implement it.

There are many ways to get to the New Issue form from the project’s page:

-   Navigate to your **Project’s Dashboard** > **Issues** > **New Issue**: **Picture to be added**
- From your **Project’s Dashboard**, click the plus sign (**+**) to open a dropdown menu with a few options. Select **New Issue** to create an issue in that project: **Picture to be added**

Henceforth, once you get access to the issues form, do remember to add a label to the issue using the **Labels** section of the sidebar.
## Working on the issue
📐 Once the discussions and implementation has been discussed, you may start working on the contribution. For which you can follow the upcoming instructions.
### Setting up your remote

Download the project repository and set up the build environment.

- Click the fork button in the project repository. This will create a new copy of a repo with the URL:
```
https://gitlab.aicrowd.com/<user-name>/evaluation-metrics-book
```
- Clone the repository by running the following command:
```
git clone git@gitlab.aicrowd.com:<user-name>/evaluation-metrics-book.git
```
- Build the repository in your local computer by running the following commands:
```
cd evaluation-metrics-book
pip install -r requirements.txt
```
- Create a new remote for the upstream remote with the command:
```
git remote add upstream https://gitlab.aicrowd.com/aicrowd/evaluation-metrics-book
```
### Building the site

Start a local http server to view the site locally.

```
jupyter-book .

cd _build/html/
python -m http.server

# Now, point your browser to localhost:8000
```
### Making a contribution

- If you would like to make changes to an existing metric, edit the relevant metric in metrics/ folder. 
- Use the sample metric template `sample.MD` in the home folder to add a new metric.  
- The new metric should be added to the `metrics/` .Add the relevant images must be present to the `images/` folder. 
- Ensure that your changes are properly reflected using the build instructions in the above section.
## Creating a Merge Request
💪 Once you are confident in your contribution and want to finally merge it to the project, you can send in a merge request through your fork. Let us look at how you can do that.
### Adding your contributions

- Create a new branch by issuing the command:
```
git checkout -b new_metric_branch
```
- Make your changes, and add and commit them
```
git add metrics/new_metric.md
git commit -m "Add metric new_metric"
git push origin new_metric_branch
```
### Creating a merge request

- Navigate to the project repository [here](https://gitlab.aicrowd.com/aicrowd/evaluation-metrics-book).
- Use the `Create merge Request` button on the top right corner of the page and open a new merge request. 
- Choose the relevant source branch and set the target branch as `master` in `aicrowd/evaluations-metrics-book`.  
- Fill in the Title and Description appropriate to your contributions. 
- Submit your Pull Request for review.
- After that, we may have questions, check back on your PR to keep up with the conversation.
## Your PR is Merged!
👏Congratualtions! The whole AIcrowd Community thanks you!🎊<br>
Once your Pull Request is merged, you will be proudly listed as a contributor in the [Contributors List](https://gitlab.aicrowd.com/aicrowd/evaluation-metrics-book/-/graphs/master).
## Keep Contributing to AIcrowd Community
👫 Now that you are a part of the AIcrowd Community with your contribution to [Evalution Metrics](https://evaluation-metrics.aicrowd.com/intro.html), how about learning about more ways to contribute to the community?
Check out the following ways you can contribute to the community:
- [Discourse](https://discourse.aicrowd.com/): AIcrowd Discourse serves as a platform for all of the community to get together and have discussions .<br> If you need assistance solving a metrics PR you're working on, have a fantastic new idea, or want to share something awesome you've learned while scouting through Evalution Metrics join us on Discourse.
- [Issues](https://gitlab.aicrowd.com/aicrowd/evaluation-metrics-book/-/issues): Issues are used to keep track of tasks that contributors might be able to assist with.
If we however haven't reviewed an issue yet, don't start working on it.
